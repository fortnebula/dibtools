# This file is a template, and might need editing before it works on your project.
FROM ubuntu:bionic

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install software-properties-common -y
RUN add-apt-repository cloud-archive:ussuri -y
RUN apt-get install -y git yum-utils rpm sudo curl git cpio squashfs-tools python3-pip xfsprogs gdisk dosfstools ca-certificates qemu-block-extra qemu-utils readline-common shared-mime-info sharutils tzdata udev uuid-runtime wget xdg-user-dirs xz-utils debootstrap dmsetup ibverbs-providers jq kpartx krb5-locales libaio1 libblas3 libcurl3-gnutls libdevmapper1.02.1 libgfortran4 libgssapi-krb5-2 libibverbs1 libiscsi7 libjq1 libk5crypto3 libkeyutils1 libkmod2 libkrb5-3 libkrb5support0 liblapack3 libnghttp2-14 libnl-3-200 libnl-route-3-200 libonig4 libpsl5 libpython-stdlib libpython2.7-minimal libpython2.7-stdlib librados2 librbd1 librdmacm1 librtmp1 libssh-4 libyaml-0-2 publicsuffix pyflakes pyflakes3 python python-babel-localedata python-minimal python-pkg-resources python-pyflakes python2.7 python2.7-minimal python-pip python3-babel python3-decorator python3-entrypoints python3-flake8 python3-mccabe python3-networkx python3-numpy python3-pbr python3-pycodestyle python3-pyflakes python3-six python3-stevedore python3-tz python3-yaml qemu-block-extra qemu-utils sharutils tzdata udev wget
RUN python3 -m pip install diskimage-builder openstackclient python-swiftclient https://tarballs.opendev.org/openstack/ironic-python-agent-builder/ironic-python-agent-builder-master.tar.gz
RUN mkdir /builds
